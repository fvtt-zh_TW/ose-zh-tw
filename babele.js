Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'ose-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });        
    }
});

